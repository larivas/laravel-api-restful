<?php

use Illuminate\Database\Seeder;

// Hacemos uso del modelo User.
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('FabricanteSeeder');
        $this->call('AvionSeeder');
        // $this->call(UsersTableSeeder::class);

        // Solo queremos un unico usuario en la table, asi que truncamos primero la tabla
        // para luego rellenarla con los registros.
        User::truncate();

        // Llamamos al seeder de Users.
        $this->call('UserSeeder');
    }
}
