<?php

use Illuminate\Database\Seeder;

// Hace uso de Fabricante.
use App\Fabricante;

// Hace uso del modelo de Avion.
use App\Avion;

// Le indicamos que utilice tambien Faker.
// Informacion sobre Faker: https://github.com/fzaninotto/Faker
use Faker\Factory as Faker;

class AvionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Creamos una instancia de Faker
        $faker = Faker::create();

        // Para cubrir los aviones tenemos que tener en cuenta que fabricantes tenemos.
        // para que la clave foranea no de problemas.
        // Averiguamos cuantos fabricantes hay en la tabla.
        $cuantos = Fabricante::all()->count();

        // Creamos un bucle para cubrir 20 aviones:
        for ($i=0; $i<20; $i++)
        {
            // Cuando llamamos al metodo de create del Modelo Avion
            // se esta creando una nueva fila en la tabla.
            Avion::create(
                [
                    'modelo'=>$faker->word(),
                    'longitud'=>$faker->randomFloat(2, 10, 150),
                    'capacidad'=>$faker->randomNumber(3),       // de 3 digitos como maximo
                    'velocidad'=>$faker->randomNumber(4),
                    'alcance'=>$faker->randomNumber(),
                    'fabricante_id'=>$faker->numberBetween(1, $cuantos)
                ]
            );
        }
    }
}
