<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Generalmente cada vez que creamos una clase tenemos que indicar el espacio de nombres
// dónde la estamos creando y suele coincidir con el nombre del directorio.
// El nombre del namespace debe comenzar por UNA LETRA MAYÚSCULA.

// Para más información ver contenido clase Model.php (CTRL + P en Sublime) de Eloquent para ver los atributos disponibles.
// Documentación completa de Eloquent ORM en: http://laravel.com/docs/5.0/eloquent

class Fabricante extends Model
{
    // Nombre de la tabla en MySQL.
    protected $table = "fabricantes";

    // Atributos que se puede asignar de manera masiva.
    protected $fillable = array ('nombre','direccion','telefono');

    // Aqui ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['created_at','updated_at'];

    // Definimos a continuacion la relacion de esta tabla con otras.
    // Ejemplos de relaciones:
    // 1 usuario tiene un telefono          -> hasOne()     Relacion 1:1
    // 1 telefono pertenece a 1 usuario     -> belongsTo()  Relacion 1:1 inversa a hasOne()
    // 1 post tiene muchos comentarios      -> hasMany()    Relacion 1:N
    // 1 comentario pertenece a 1 post      -> belongsTo()  Relacion 1:N inversa a hanMany()
    // 1 usuario puede tener muchos roles   -> belongsToMany()

    // Relacion de Fabricante con Aviones:
    public function aviones()
    {
        // 1 fabricante tiene muchos aviones
        return $this->hasMany('App\Avion');
    }
}
