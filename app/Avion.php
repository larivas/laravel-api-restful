<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Generalmente cada vez que creamos una clase tenemos que indicar el espacio de nombres
// dónde la estamos creando y suele coincidir con el nombre del directorio.
// El nombre del namespace debe comenzar por UNA LETRA MAYÚSCULA.

// Para más información ver contenido clase Model.php (CTRL + P en Sublime) de Eloquent para ver los atributos disponibles.
// Documentación completa de Eloquent ORM en: http://laravel.com/docs/5.0/eloquent

class Avion extends Model
{
    // Nombre de la tabla en MySQL.
    protected $table = 'aviones';

    // Eloquent asume que cada tabla tiene una clave primaria on una columna llamda id.
    // si este no fuera el caso entonces hay que indicar cual es nuestra clave primaria en la tabla:
    protected $primaryKey = 'serie';

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = array ('modelo','longitud','capacidad','velocidad','alcance','fabricante_id');

    // Aqui se colocan los campos que no se desean devulver en las consultas
    protected $hidden = ['created_at','updated_at'];

    // Definimos a continuacion la relacion de esta tabla con otras.
    // Ejemplos de relaciones:
    // 1 usuario tiene un telefono          -> hasOne()     Relacion 1:1
    // 1 telefono pertenece a 1 usuario     -> belongsTo()  Relacion 1:1 inversa a hasOne()
    // 1 post tiene muchos comentarios      -> hasMany()    Relacion 1:N
    // 1 comentario pertenece a 1 post      -> belongsTo()  Relacion 1:N inversa a hanMany()
    // 1 usuario puede tener muchos roles   -> belongsToMany()

    // Relacion de Avion con Fabricante
    public function fabricante()
        {
            // 1 avion pertenece a un Fabricante
            // $this hace referencia al objeto que tenemos en ese momento de Avion.
            return $this->belongs('App\Fabricante');
        }
}
